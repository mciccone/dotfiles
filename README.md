# dotfiles
My dotfiles collection


### Install Neovim
pip install pylon proselint flake8 pynvim neovim neovim-remote

apt install ctags xdotool

- Latest tmux is necessary, to have right term colors
http://witkowskibartosz.com/blog/update-your-tmux-to-latest-version.html

- how to test colors in your terminal
https://gist.github.com/XVilka/8346728

- install nerd fonts
[GitHub - ryanoasis/nerd-fonts: Iconic font aggregator, collection, & patcher. 3,600+ icons, 40+ patched fonts: Hack, Source Code Pro, more. Glyph collections: Font Awesome, Material Design Icons, Octicons, & more](https://github.com/ryanoasis/nerd-fonts)

- install the_silver_searcher
[Faster Grepping in Vim](https://thoughtbot.com/blog/faster-grepping-in-vim)
